import { Injectable } from '@angular/core';
import * as socketIo from 'socket.io-client';
import { Observable, Subject } from 'rxjs';


@Injectable( {
    providedIn: null
} )
export class SocketService {

    private socket;


    private messageObservable$: Subject<Array<{ id: string, message: string }>> = new Subject();
    public messageObservableChanged$: Observable<Array<{ id: string, message: string }>> = this.messageObservable$.asObservable();

    constructor() {
        this.socket = socketIo( 'http://localhost:1234' );
        this.socket.on( 'allMessages', msg => {
            this.messageObservable$.next( msg );
        } );
    }


    sendMessage( msg: string ) {
        this.socket.emit( 'sendMessage', { id: this.makeid( 5 ), message: msg } );
    }

    removeMessage( id: string ) {
        this.socket.emit( 'removeMessage', id );
    }

    private makeid( length ): string {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt( Math.floor( Math.random() * charactersLength ) );
        }
        return result;
    }

}
