import { Component, OnInit } from '@angular/core';
import { SocketService } from './modules/data/services/socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  protected messageList: Array<{ id: string, message: string }> = [];

  protected message: string;

  constructor( private socketService: SocketService ) {
  }

  ngOnInit() {
    this.socketService.messageObservableChanged$.subscribe( ( list ) => {
      this.messageList = list;
    } );
  }

  protected sendMessage() {
    this.socketService.sendMessage( this.message );
    this.message = null;
  }

  protected removeMsg( id: string ) {
    this.socketService.removeMessage( id );
  }
}

