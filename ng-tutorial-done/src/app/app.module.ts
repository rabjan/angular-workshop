import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataModule } from './modules/data/data.module';
import { CoreModule } from './modules/core/core.module';
import { PageAComponent } from './pages/page-a/page-a.component';
import { PageBComponent } from './pages/page-b/page-b.component';
import { PageAAComponent } from './pages/page-a/page-a-a/page-a-a.component';
import { PageABComponent } from './pages/page-a/page-a-b/page-a-b.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import counterReducer from './modules/data/ngrx/reducers/counter.reducer';
import userReducer from './modules/data/ngrx/reducers/user.reducer';

@NgModule( {
    declarations: [
        AppComponent,
        PageAComponent,
        PageBComponent,
        PageAAComponent,
        PageABComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        DataModule,
        CoreModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot( {
            counter: counterReducer,
            userList: userReducer
        } )
    ],
    providers: [],
    bootstrap: [
        AppComponent
    ]
} )
export class AppModule {
}
