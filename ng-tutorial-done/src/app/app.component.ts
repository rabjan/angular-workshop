import { Component } from '@angular/core';
import { HttpService } from './modules/data/services/http.service';
import { CountryModel } from './modules/data/models/country-model';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { StoreState } from './modules/data/ngrx/store.state';
import { Observable } from 'rxjs';
import { DecrementCounter, IncrementCounter, ResetCounter } from './modules/data/ngrx/actions/counter.actions';
import { UserModel } from './modules/data/ngrx/models/user.model';
import { AddUser, RemoveUser } from './modules/data/ngrx/actions/user.actions';

@Component( {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [ './app.component.less' ]
} )
export class AppComponent {

    protected title = 'ng-tutorial-done';

    // Template Driven Form
    protected model: CountryModel;

    // Reactive Form
    protected modelGroup: FormGroup;

    // Dynamic Form
    protected modelDynamicGroup: FormGroup;

    // NgRx
    protected counter$: Observable<number>;
    protected userList$: Observable<UserModel[]>;

    protected user: UserModel = {
        firstName: null,
        lastName: null
    };


    constructor( private httpService: HttpService,
                 private formBuilder: FormBuilder,
                 private store: Store<StoreState> ) {
        // 1
        // this.httpService.getCountries()
        //     .then( ( data ) => {
        //         console.log( data );
        //     } )
        //     .catch( reason => {
        //         console.warn( reason );
        //     } );

        // 2
        // this.httpService.getCountriesInModel()
        //     .then( ( data ) => {
        //         console.log( data, data.getCountryByCode( 'CZ' ) );
        //         this.model = data.getCountryByCode( 'CZ' );
        //
        //         this.modelGroup = this.formBuilder.group( {
        //             name: [ this.model.name ],
        //             alpha2Code: [ this.model.aplha2Code ],
        //             population: [ this.model.population ]
        //         } );
        //
        //         this.modelDynamicGroup = this.toDynamicForm( data.getCountryByCode( 'CZ' ) );
        //
        //
        //     } )
        //     .catch( reason => {
        //         console.warn( reason );
        //     } );

        // 3
        // let rxJSObservablesInstance = new RxJSObservables();
        //
        // rxJSObservablesInstance.setStringSubjectObservable( '1 cba' );
        // rxJSObservablesInstance.stringSubjectObservableChanged$.subscribe( ( state ) => {
        //     console.log( state );
        // } );
        // rxJSObservablesInstance.setStringSubjectObservable( '1 abc' );
        //
        // rxJSObservablesInstance.setstringBehaviourSubjectObservable( '2 cba' );
        // rxJSObservablesInstance.stringBehaviourSubjectObservableChanged$.subscribe( ( state ) => {
        //     console.log( state );
        // } );
        // rxJSObservablesInstance.setstringBehaviourSubjectObservable( '2 abc' );


        // 4
        this.counter$ = this.store.pipe( select( 'counter' ) );
        this.userList$ = this.store.pipe( select( 'userList' ) );

    }

    protected onSubmit() {
        console.log( 'submitted', this.model );
    }

    protected onSubmitReactive() {
        console.log( 'submitted from reactive', this.modelGroup.getRawValue() );
    }

    protected onSubmitDynamic() {
        console.log( 'submitted from Dynamic', this.modelDynamicGroup.getRawValue() );
    }

    private toDynamicForm( data ) {
        let group: any = {};

        Object.keys( data ).forEach( ( key ) => {
            group[ key.replace( '_', '' ) ] = new FormControl( data[ key.replace( '_', '' ) ] );
        } );


        return new FormGroup( group );
    }

    private increment() {
        this.store.dispatch( new IncrementCounter() );
    }

    private decrement() {
        this.store.dispatch( new DecrementCounter() );
    }

    private reset() {
        this.store.dispatch( new ResetCounter() );
    }

    private removeUser( index: number ) {
        this.store.dispatch( new RemoveUser( index ) );
    }

    private addUser() {
        this.store.dispatch( new AddUser( this.user ) );
        this.user = {
            firstName: null,
            lastName: null
        };
    }
}
