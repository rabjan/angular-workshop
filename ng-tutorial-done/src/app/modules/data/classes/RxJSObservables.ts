import { BehaviorSubject, Observable, Subject } from 'rxjs';

export class RxJSObservables {

    private stringSubjectObservable$: Subject<string> = new Subject();
    public stringSubjectObservableChanged$: Observable<string> = this.stringSubjectObservable$.asObservable();

    setStringSubjectObservable( stringSubject: string ) {
        this.stringSubjectObservable$.next( stringSubject );
    }

    // BehaviourSubject (potrebuje defaultni hodnotu)
    // ReplaySubject (prochazi vsechny zmenene hodnoty)

    private stringBehaviourSubjectObservable$: BehaviorSubject<string> = new BehaviorSubject( 'must have' );
    public stringBehaviourSubjectObservableChanged$: Observable<string> = this.stringBehaviourSubjectObservable$.asObservable();

    setstringBehaviourSubjectObservable( stringBehaviourSubject: string ) {
        this.stringBehaviourSubjectObservable$.next( stringBehaviourSubject );
    }


//     let rxJSObservablesInstance = new RxJSObservables();
//
//     rxJSObservablesInstance.setStringSubjectObservable( '1 cba' );
//     rxJSObservablesInstance.stringSubjectObservableChanged$.subscribe( ( state ) => {
//          console.log( state );
//     } );
//     rxJSObservablesInstance.setStringSubjectObservable( '1 abc' );
//
//     rxJSObservablesInstance.setstringBehaviourSubjectObservable('2 cba' );
//     rxJSObservablesInstance.stringBehaviourSubjectObservableChanged$.subscribe( ( state ) => {
//         console.log( state );
//     } );
//     rxJSObservablesInstance.setstringBehaviourSubjectObservable('2 abc' );
}
