import { TestBed } from '@angular/core/testing';

import { LoadMessageResolver } from './load-message-resolver.service';

describe('LoadMessageResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadMessageResolver = TestBed.get(LoadMessageResolver);
    expect(service).toBeTruthy();
  });
});
