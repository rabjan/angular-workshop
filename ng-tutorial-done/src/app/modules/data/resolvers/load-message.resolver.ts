import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

@Injectable( {
    providedIn: null
} )
export class LoadMessageResolver implements Resolve<Promise<string>> {

    constructor() {
    }

    resolve(): Promise<string> {
        return new Promise( ( res, rej ) => {
            setTimeout( () => {
                res( 'I am loaded' );
            }, 2000 );
        } );
    }


}
