import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { LoadMessageResolver } from './resolvers/load-message.resolver';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './services/http.service';

@NgModule( {
    declarations: [],
    providers: [
        AuthGuard,
        AuthService,
        LoadMessageResolver,
        HttpService
    ],
    imports: [
        CommonModule,
        HttpClientModule
    ]
} )
export class DataModule {
}
