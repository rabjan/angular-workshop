import { CountryModel } from './country-model';

export class CountryListModel {
    private countryList: Array<CountryModel> = [];

    public getCountryList(): Array<CountryModel> {
        return this.countryList;
    }

    public appendCountryItem( countryModel: CountryModel ) {
        this.countryList.push( countryModel );
    }

    public getCountryByCode( alpha2Code: string ): CountryModel {
        return this.countryList.find( ( country ) => country.aplha2Code == alpha2Code );
    }
}
