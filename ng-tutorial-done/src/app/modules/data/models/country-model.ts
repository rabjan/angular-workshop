export class CountryModel {

    private _name: string;
    private _aplha2Code: string;
    private _population: number;


    constructor( name: string, aplha2Code: string, population: number ) {
        this._name = name;
        this._aplha2Code = aplha2Code;
        this._population = population;
    }


    get name(): string {
        return this._name;
    }

    set name( value: string ) {
        this._name = value;
    }

    get aplha2Code(): string {
        return this._aplha2Code;
    }

    set aplha2Code( value: string ) {
        this._aplha2Code = value;
    }

    get population(): number {
        return this._population;
    }

    set population( value: number ) {
        this._population = value;
    }

    getDescription(): string {
        return this.name + ' - [' + this.aplha2Code + '] - ' + this.population;
    }
}
