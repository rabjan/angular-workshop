import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable( {
    providedIn: null
} )
export class AuthGuard implements CanActivate {

    constructor( private authService: AuthService,
                 private router: Router ) {
    }

    canActivate(): boolean {
        if ( !this.authService.isAuthenticated() ) {
            this.router.navigate( [ 'home' ] );
            console.warn( 'You shall not pass' );
            return false;
        }
        return true;
    }


}
