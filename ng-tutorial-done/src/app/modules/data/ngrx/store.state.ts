import { UserModel } from './models/user.model';

export interface StoreState {
    readonly counter: number;
    readonly userList: UserModel[];
}
