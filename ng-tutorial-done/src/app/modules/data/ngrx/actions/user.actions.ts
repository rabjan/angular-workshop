import { Action } from '@ngrx/store';
import { UserModel } from '../models/user.model';

export enum UserActionsTypes {
    ADD_USER = '[USER] Add',
    REMOVE_USER = '[USER] Remove',
}


export class AddUser implements Action {
    readonly type = UserActionsTypes.ADD_USER;

    constructor( public user: UserModel ) {
    }
}


export class RemoveUser implements Action {
    readonly type = UserActionsTypes.REMOVE_USER;

    constructor( public index: number ) {
    }
}
