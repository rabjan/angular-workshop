import { Action } from '@ngrx/store';

export enum CounterActionsTypes {
    INCREMENT_COUNTER = '[Counter] Increment',
    DECREMENT_COUNTER = '[Counter] Decrement',
    RESET_COUNTER = '[Counter] Reset',
}


export class IncrementCounter implements Action {
    readonly type = CounterActionsTypes.INCREMENT_COUNTER;
}

export class DecrementCounter implements Action {
    readonly type = CounterActionsTypes.DECREMENT_COUNTER;
}

export class ResetCounter implements Action {
    readonly type = CounterActionsTypes.RESET_COUNTER;
}
