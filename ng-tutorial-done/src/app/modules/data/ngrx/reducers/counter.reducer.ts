import { Action } from '@ngrx/store';
import { CounterActionsTypes } from '../actions/counter.actions';

export const initialState = 0;

export default function counterReducer( state = initialState, action: Action ) {
    switch ( action.type ) {
        case CounterActionsTypes.INCREMENT_COUNTER:
            return state + 1;

        case CounterActionsTypes.DECREMENT_COUNTER:
            return state - 1;

        case  CounterActionsTypes.RESET_COUNTER:
            return 0;

        default:
            return state;
    }

}
