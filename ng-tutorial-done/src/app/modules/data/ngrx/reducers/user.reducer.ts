import { Action } from '@ngrx/store';
import { AddUser, RemoveUser, UserActionsTypes } from '../actions/user.actions';
import { UserModel } from '../models/user.model';

export const initialState: UserModel[] = [
    {
        firstName: 'Petr',
        lastName: 'Novak'
    },
    {
        firstName: 'Petra',
        lastName: 'Novakova'
    }
];

export default function userReducer( state: UserModel[] = initialState, action: Action ) {
    switch ( action.type ) {
        case UserActionsTypes.ADD_USER:
            return [...state, (action as AddUser).user];

        case UserActionsTypes.REMOVE_USER:
            state.splice((action as RemoveUser).index, 1);
            return state;

        default:
            return state;
    }

}
