import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICountryResponse } from '../interfaces/country-response';
import { CountryListModel } from '../models/country-list-model';
import { CountryModel } from '../models/country-model';
import { Observable } from 'rxjs';

@Injectable( {
    providedIn: 'root'
} )
export class HttpService {

    constructor( private httpClient: HttpClient ) {

    }

    public getCountries(): Promise<Array<ICountryResponse>> {
        return new Promise( ( resolve, reject ) => {
            this.httpClient.get( 'https://restcountries.eu/rest/v2/region/europe' )
                .toPromise()
                .then( ( data: Array<any> ) => {
                    const countryResponseList: Array<ICountryResponse> = [];
                    data.forEach( ( item ) => {
                        countryResponseList.push( {
                            name: item.name,
                            alpha2Code: item.alpha2Code,
                            population: item.population
                        } );
                    } );

                    resolve( countryResponseList );
                } )
                .catch( reason => {
                    reject( reason );
                } );
        } );

    }

    // Use Dynamic Model Builder Prototype


    public getCountriesInModel(): Promise<CountryListModel> {


        return new Promise( ( resolve, reject ) => {
            this.httpClient.get( 'https://restcountries.eu/rest/v2/region/europe' )
                .toPromise()
                .then( ( data: Array<any> ) => {
                    const countryResponseList = new CountryListModel();

                    data.forEach( ( item ) => {
                        countryResponseList.appendCountryItem( new CountryModel( item.name, item.alpha2Code, item.population ) );
                    } );

                    resolve( countryResponseList );
                } )
                .catch( reason => {
                    reject( reason );
                } );
        } );

    }
}
