export interface ICountryResponse {
    alpha2Code: string;
    name: string;
    population: number;
}
