import { TestBed } from '@angular/core/testing';

import { CoreServiceBService } from './core-service-b.service';

describe('CoreServiceBService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoreServiceBService = TestBed.get(CoreServiceBService);
    expect(service).toBeTruthy();
  });
});
