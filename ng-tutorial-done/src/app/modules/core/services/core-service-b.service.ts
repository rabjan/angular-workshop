import { Injectable } from '@angular/core';
import { ICoreServiceB } from '../interfaces/core-service-b';

@Injectable( {
    providedIn: 'root'
} )
export class CoreServiceBService implements ICoreServiceB {

    constructor() {
    }

    mustImplementThis(): void {
    }

    public implementedWithoutInterface(): void {

    }

}
