import { TestBed } from '@angular/core/testing';

import { CoreServiceAService } from './core-service-a.service';

describe('CoreServiceAService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoreServiceAService = TestBed.get(CoreServiceAService);
    expect(service).toBeTruthy();
  });
});
