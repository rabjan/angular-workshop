import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponentAComponent } from './components/core-component-a/core-component-a.component';
import { CoreServiceAService } from './services/core-service-a.service';
import { HighlightDirective } from './directives/highlight.directive';
import { PowerPipe } from './pipes/power.pipe';

@NgModule( {
  declarations: [ CoreComponentAComponent, HighlightDirective, PowerPipe ],
  exports: [
    CoreComponentAComponent
  ],
  providers: [
      CoreServiceAService
  ],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
