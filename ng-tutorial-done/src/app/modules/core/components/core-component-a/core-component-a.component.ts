import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-core-component-a',
  templateUrl: './core-component-a.component.html',
  styleUrls: ['./core-component-a.component.less']
})
export class CoreComponentAComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
