import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreComponentAComponent } from './core-component-a.component';

describe('CoreComponentAComponent', () => {
  let component: CoreComponentAComponent;
  let fixture: ComponentFixture<CoreComponentAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreComponentAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreComponentAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
