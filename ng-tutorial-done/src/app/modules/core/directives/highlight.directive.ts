import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive( {
    selector: '[appHighlight]'
} )
export class HighlightDirective {

    constructor( private element: ElementRef ) {
    }

    @HostListener( 'mouseenter' )
    onMouseEnter() {
        this.hightlight( 'red' );
    }


    @HostListener( 'mouseleave' )
    onMouseLeave() {
        this.hightlight( null );
    }

    private hightlight( color: string ) {
        this.element.nativeElement.style.color = color;
    }

}
