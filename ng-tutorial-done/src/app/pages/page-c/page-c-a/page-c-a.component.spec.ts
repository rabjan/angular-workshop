import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCAComponent } from './page-c-a.component';

describe('PageCAComponent', () => {
  let component: PageCAComponent;
  let fixture: ComponentFixture<PageCAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
