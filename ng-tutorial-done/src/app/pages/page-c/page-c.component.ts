import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component( {
    selector: 'app-page-c',
    templateUrl: './page-c.component.html',
    styleUrls: [ './page-c.component.less' ]
} )
export class PageCComponent implements OnInit {

    constructor( private activatedRoute: ActivatedRoute ) {
        this.activatedRoute.data.subscribe( ( data ) => {
            console.log( 'changed data', data );
        } );
    }

    ngOnInit() {
    }

}
