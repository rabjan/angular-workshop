import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulePageCRoutingModule } from './module-page-c-routing.module';
import { PageCAComponent } from './page-c-a/page-c-a.component';
import { PageCBComponent } from './page-c-b/page-c-b.component';
import { PageCComponent } from './page-c.component';

@NgModule( {
    declarations: [
        PageCComponent,
        PageCAComponent,
        PageCBComponent
    ],
    imports: [
        CommonModule,
        ModulePageCRoutingModule
    ]
} )
export class ModulePageCModule {
}
