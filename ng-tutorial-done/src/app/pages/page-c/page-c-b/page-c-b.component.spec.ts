import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCBComponent } from './page-c-b.component';

describe('PageCBComponent', () => {
  let component: PageCBComponent;
  let fixture: ComponentFixture<PageCBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
