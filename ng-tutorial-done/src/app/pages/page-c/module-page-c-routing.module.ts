import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageCAComponent } from './page-c-a/page-c-a.component';
import { PageCBComponent } from './page-c-b/page-c-b.component';
import { PageCComponent } from './page-c.component';
import { LoadMessageResolver } from '../../modules/data/resolvers/load-message.resolver';

const routes: Routes = [
    {
        path: '',
        component: PageCComponent,
        resolve: {
            loadedMessage: LoadMessageResolver
        },
        children: [
            {
                path: 'a',
                component: PageCAComponent
            },
            {
                path: 'b',
                component: PageCBComponent
            }
        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class ModulePageCRoutingModule {
}
