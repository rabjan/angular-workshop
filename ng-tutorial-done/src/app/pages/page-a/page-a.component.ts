import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { PageABComponent } from './page-a-b/page-a-b.component';
import { CoreServiceAService } from '../../modules/core/services/core-service-a.service';
import { CoreServiceBService } from '../../modules/core/services/core-service-b.service';
import { ICoreServiceB } from '../../modules/core/interfaces/core-service-b';

@Component( {
    selector: 'app-page-a',
    templateUrl: './page-a.component.html',
    styleUrls: [ './page-a.component.less' ]
} )
export class PageAComponent implements OnInit {

    @ViewChild( PageABComponent ) childAB: PageABComponent;

    protected amount: number = 6;

    protected showAB: boolean = true;

    constructor( private coreServiceA: CoreServiceAService,
                 @Inject( CoreServiceBService ) private coreServiceB: ICoreServiceB,
                 private coreServiceBAgain: CoreServiceBService ) {
        this.amount = this.coreServiceA.getAmount();

        this.coreServiceB.mustImplementThis();
        this.coreServiceBAgain.implementedWithoutInterface();
    }

    ngOnInit() {

        // @ts-ignore
        setTimeout( () => {
            this.showAB = false;
        }, 2000 );
    }

    protected updatedAAmount( value: number ): void {
        this.amount = value;
    }

    protected updateBValue( value: string ): void {
        this.childAB.callFromOutside.emit( value );
    }


}
