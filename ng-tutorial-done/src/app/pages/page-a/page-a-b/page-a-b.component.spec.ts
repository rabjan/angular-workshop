import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageABComponent } from './page-a-b.component';

describe('PageABComponent', () => {
  let component: PageABComponent;
  let fixture: ComponentFixture<PageABComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageABComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageABComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
