import { Component, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';

@Component( {
    selector: 'app-page-a-b',
    templateUrl: './page-a-b.component.html',
    styleUrls: [ './page-a-b.component.less' ]
} )
export class PageABComponent implements OnInit, OnDestroy {

    @Input()
    protected name: string;

    @Input()
    public callFromOutside: EventEmitter<string> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
        this.callFromOutside.subscribe( ( newValue ) => {
            this.name = newValue;
        } );
    }


    ngOnDestroy(): void {

        // @ts-ignore
        console.log( 'i am destroyed' );
    }


}
