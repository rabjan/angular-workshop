import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component( {
    selector: 'app-page-a-a',
    templateUrl: './page-a-a.component.html',
    styleUrls: [ './page-a-a.component.less' ]
} )
export class PageAAComponent implements OnInit {

    @Input()
    protected amount: number = 0;

    @Output()
    public changedAmountFromChild: EventEmitter<number> = new EventEmitter();

    constructor() {
        // @ts-ignore
        console.log(this.amount);
    }

    ngOnInit() {
        // @ts-ignore
        console.log(this.amount);
    }

    protected changedAmount( amount: number ) {
        this.changedAmountFromChild.emit( amount );
    }

}
