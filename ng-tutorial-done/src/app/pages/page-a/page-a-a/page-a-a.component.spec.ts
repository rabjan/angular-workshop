import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAAComponent } from './page-a-a.component';

describe('PageAAComponent', () => {
  let component: PageAAComponent;
  let fixture: ComponentFixture<PageAAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
