import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAComponent } from './pages/page-a/page-a.component';
import { PageBComponent } from './pages/page-b/page-b.component';
import { AuthGuard } from './modules/data/guards/auth.guard';

const routes: Routes = [
    {
        path: 'home',
        component: PageAComponent
    },
    {
        path: 'page2',
        component: PageBComponent
    },
    {
        path: 'page3',
        loadChildren: './pages/page-c/module-page-c.module#ModulePageCModule',
        canActivate: [
            AuthGuard
        ]
    }
    // {
    //     path: '**',
    //     redirectTo: 'home'
    // }
];

@NgModule( {
    imports: [ RouterModule.forRoot( routes ) ],
    exports: [ RouterModule ]
} )
export class AppRoutingModule {
}
