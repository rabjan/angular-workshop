const express = require('express');
const app = express();
const path = require('path');
const http = require('http').Server(app);
const io = require('socket.io')(http);

const messages = [];


app.use(express.static(__dirname + '/dist/ng-demo-app-done'));


app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname));
});

io.on("connection", socket => {
    console.log("new connection", socket.id)

    socket.on("sendMessage", message => {
        messages.push(message);
        io.emit("allMessages", messages);
    });

    socket.on("removeMessage", id => {
        let index = messages.findIndex(message => message.id == id);
        messages.splice(index, 1);
        io.emit("allMessages", messages);
    });

    // socket pouze konkretnimu spojeni
    // io vsem
    io.emit("allMessages", messages);
});

http.listen(1234);
